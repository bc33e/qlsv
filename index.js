var dssv = [];
var BASE_URL = "https://62f8b7523eab3503d1da1597.mockapi.io";

var turnOnLoading = function(){
  document.getElementById("loading").style.display = "flex"
};

var turnOffLoading = function(){
  document.getElementById("loading").style.display = "none"
};

var renderTable = function (list) {
  var contentHtml = "";
  list.forEach(function (item) {
    var trContent = `
    <tr>
    <td>${item.ma}</td>
    <td>${item.ten}</td>
    <td>${item.email}</td>
    <td><img src=${item.hinhAnh} style="width:80px"</td>
    <td>
    <button onclick="suaSinhVien(${item.ma})" class="btn btn-info">Sửa</button>
    <button onclick="xoaSinhVien(${item.ma})" class="btn btn-danger">Xóa</button>
    </td>
    </tr>`;
    contentHtml += trContent;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHtml;
};

// LẤY THÔNG TIN RA BẢNG
var renderDssvService = function(){
  turnOnLoading()
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading()
      dssv = res.data;
      console.log("dssv: ", dssv);
      renderTable(dssv);
    })
    .catch(function (err) {
      console.log(err);
      turnOffLoading()
    });
}

// XÓA SV
renderDssvService();
function xoaSinhVien(id){
  console.log("id: ", id)
  turnOnLoading()
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
  .then(function (res) {
    turnOffLoading()
    renderDssvService();
    console.log("res: ", res);
    
  })
  .catch(function (err) {
    console.log(err);
    turnOffLoading()
  });
}

// THÊM SV
function themSV(){
  var dataForm = layThongTinTuForm();
  // console.log('dataSv: ', dataSv);
  turnOnLoading()
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: dataForm
  })
  
  .then(function(res){
    turnOffLoading();
    renderDssvService();
    console.log("res: ", res);
    
  })
  .catch(function (err) {
    console.log(err);
    turnOffLoading()
  });
}

// SỬA SV
function suaSinhVien(id){
  turnOnLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
  .then(function(res){
    turnOffLoading();
    console.log(res);
    hienThiThongTin(res.data)
  })
  .catch(function(err){
    turnOffLoading();
    console.log(err)
  })
}

// CẬP NHẬT SV
function capNhatSV(){
  var dataSv = layThongTinTuForm();
  console.log('dataSv: ', dataSv);
  turnOnLoading();

  axios({
    url: `${BASE_URL}/sv/${dataSv.ma}`,
    method: "PUT",
    data: dataForm,
  })
  .then(function(res){
    console.log(res)
    turnOffLoading();
    renderDssvService()
  })
  .catch(function(err){
    console.log(err);
    turnOffLoading();
    
  })

}
